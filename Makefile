bin := $(shell pwd)/tools/bin

customers_proto := rpc/customers/service.proto

.PHONY: tools
tools:
	@echo "Downloading and installing proto generators..."
	@GOBIN=$(bin) go install github.com/twitchtv/twirp/protoc-gen-twirp
	@GOBIN=$(bin) go install google.golang.org/protobuf/cmd/protoc-gen-go
	@go get github.com/go-bridget/twirp-swagger-gen/cmd/protoc-gen-twirp-swagger	
	@GOBIN=$(bin) go install github.com/go-bridget/twirp-swagger-gen/cmd/protoc-gen-twirp-swagger
	@GOBIN=$(bin) go install github.com/go-bridget/twirp-swagger-gen/cmd/twirp-swagger-gen

.PHONY: pb
pb-customers:
	@echo "Generating customers go-twirp servers and clients..."
	@PATH=$(PATH):$(bin) protoc \
		--go_out=paths=source_relative:. \
		--twirp_out=paths=source_relative:. \
		$(customers_proto)

.PHONY: swagg
swagger-customers:
	@echo "Generating customers swagger definitions..."
	@PATH=$(PATH):$(bin) twirp-swagger-gen \
     	-in $(customers_proto) \
     	-out swagger/customers.swagger.json \
     	-host localhost:8888

.PHONY: deploy-swagg
deploy-swagg:
	@echo "Building apidocs image..."
	@echo "Pushing apidocs image..."
	@echo "Deploying apidocs..."

.PHONY: clients
clients:
	@echo Generating Javascript clients...
	@echo Deploying clients...
