package customers_test

import (
	"context"
	pb "gitlab.com/retail-unlimited/apihub/rpc/customers"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestProvisionCustomerContract(t *testing.T) {
	client, cleanup := setupClient()
	defer cleanup()

	pc, err := client.ProvisionCustomer(context.TODO(), &pb.CustomerToProvision{
		FirstName: "",
		LastName:  "",
		//CompanyName: "", TODO TDD
	})

	if err != nil {
		t.Fatal("no error expected")
	}

	_ = pc.Id
}

type customersMock struct{}

func (c customersMock) ProvisionCustomer(
	_ context.Context, _ *pb.CustomerToProvision) (*pb.ProvisionedCustomer, error) {
	return &pb.ProvisionedCustomer{
		Id: "id-123",
	}, nil
}

func setupClient() (pb.Customers, func()) {
	url, clean := setupServer()

	return pb.NewCustomersProtobufClient(url, http.DefaultClient), clean
}

func setupServer() (string, func()) {
	handler := pb.NewCustomersServer(customersMock{}, nil)

	s := httptest.NewServer(handler)

	return s.URL, func() {
		s.Close()
	}
}
