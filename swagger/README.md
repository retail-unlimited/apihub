# Swagger Docs
Twirp generated swagger definitions and swagger ui

# How to run 
- Generate swagger definitions from the root via: `make swagg`
- Run `make up` from this folder to run swagger-ui locally
- Run `make down` in order to stop it
- 